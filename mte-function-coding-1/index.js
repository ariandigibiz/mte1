function countLetter(letter, sentence) {
  let result = 0;

  // Check first whether the letter is a single character.
  // If the letter is a single character, count how many times a letter has occurred in a given sentence then return count.
  // If the letter is invalid, return undefined.

  if (letter.length === 1) {
    for (let i = 0; i < sentence.length; i++) {
      if (sentence[i] === letter) {
        result++;
      }
    }
    return result;
  } else {
    return undefined;
  }
}

function isIsogram(text) {
  // An isogram is a word where there are no repeating letters.
  // The function should disregard text casing before doing anything else.
  // If the function finds a repeating letter, return false. Otherwise, return true.

  const lowercaseText = text.toLowerCase();
  const newLetters = new Set();

  for (const letter of lowercaseText) {
    if (newLetters.has(letter)) {
      return false;
    }
    newLetters.add(letter);
  }

  return true;
}

function purchase(age, price) {
  if (age < 13) {
    return undefined;
  } else if ((age >= 13 && age <= 21) || age >= 60) {
    const discountedPrice = Math.floor(price * 0.20);
    return JSON.stringify(discountedPrice);
  } else if (age >= 22 && age <= 64) {
    return JSON.stringify(Math.floor(price));
  } else {
    return undefined;
  }
}




function findHotCategories(items) {
  const hotCategoriesSet = new Set();

  items.forEach((item) => {
    if (item.stocks === 0) {
      hotCategoriesSet.add(item.category);
    }
  });

  const hotCategories = Array.from(hotCategoriesSet);

  return hotCategories;
}

function findFlyingVoters(candidateA, candidateB) {
  // Find voters who voted for both candidate A and candidate B.

  const flyingVoters = [];

  candidateA.forEach((voterA) => {
    if (candidateB.includes(voterA)) {
      flyingVoters.push(voterA);
    }
  });

  return flyingVoters;
}

module.exports = {
  countLetter,
  isIsogram,
  purchase,
  findHotCategories,
  findFlyingVoters,
};
